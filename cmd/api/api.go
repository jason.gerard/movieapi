package api

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"log/slog"
	"movieapi/pkg/db"
	_ "movieapi/pkg/logging"
	"movieapi/pkg/route"
	"movieapi/pkg/service"
	"net/http"
)

func Command() *cobra.Command {
	return &cobra.Command{
		Use:   "api",
		Short: "api",
		RunE: func(cmd *cobra.Command, args []string) error {
			ctx := cmd.Context()

			config := viper.New()

			config.AutomaticEnv()
			config.SetDefault("ADDR", ":8080")

			addr := config.GetString("ADDR")

			movieDB, err := db.GetMovieDB(ctx, config)
			if err != nil {
				panic(err)
			}

			svc := service.NewMovieService(movieDB)

			mux := route.CreateMovieMux(svc)

			slog.Info("starting HTTP server", "addr", addr)
			return http.ListenAndServe(addr, mux)
		},
	}
}
