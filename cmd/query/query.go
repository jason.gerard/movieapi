package query

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"movieapi/pkg/db"
	_ "movieapi/pkg/logging"
)

func Command() *cobra.Command {
	return &cobra.Command{
		Use:   "query",
		Short: "database query tool",
		RunE: func(cmd *cobra.Command, args []string) error {
			ctx := cmd.Context()

			config := viper.New()

			config.AutomaticEnv()

			movieDB, err := db.GetMovieDB(ctx, config)
			if err != nil {
				panic(err)
			}

			m, err := movieDB.ListAll(ctx, 100, 100)
			if err != nil {
				return err
			}

			fmt.Println(m)
			return nil
		},
	}
}
