package main

import (
	"github.com/spf13/cobra"
	"movieapi/cmd/api"
	"movieapi/cmd/query"
	"movieapi/cmd/version"
)

var SemVer string

func main() {
	cmd := &cobra.Command{
		Use:   "movieapi",
		Short: "movieapi",
	}

	cmd.AddCommand(api.Command())
	cmd.AddCommand(query.Command())
	cmd.AddCommand(version.Command(SemVer))

	if err := cmd.Execute(); err != nil {
		panic(err)
	}

}
