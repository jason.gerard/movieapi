package db

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
	_ "modernc.org/sqlite"
	"movieapi/pkg/model"
)

func CreateDB(ctx context.Context, sqlitePath string) (*sqlx.DB, error) {
	schema := `create table if not exists movies (
			id integer primary key autoincrement,
			title text,
			year integer
		)
		strict;`

	db, err := sqlx.Open("sqlite", sqlitePath)
	if err != nil {
		return nil, fmt.Errorf("unable to open sqlite file %s: %w", sqlitePath, err)
	}
	_, err = db.ExecContext(ctx, schema)
	if err != nil {
		return nil, fmt.Errorf("unable to execute schema query: %w", err)
	}

	return db, err
}

func GetMovieDB(ctx context.Context, config *viper.Viper) (*MovieDB, error) {
	p := config.GetString("SQLITE_PATH")
	db, err := CreateDB(ctx, p)

	if err != nil {
		return nil, fmt.Errorf("unable to get SQLite DB at %s: %w", p, err)
	}

	return &MovieDB{
		DB: db,
	}, nil
}

type MovieDB struct {
	DB *sqlx.DB
}

func (mdb *MovieDB) ListAll(ctx context.Context, limit, offset int) ([]model.Movie, error) {

	var movies []model.Movie

	qry := "select * from movies"

	if limit > 0 {
		qry = fmt.Sprintf("%s limit %v", qry, limit)
	}

	if offset > 0 {
		qry = fmt.Sprintf("%s offset %v", qry, offset)
	}

	err := mdb.DB.SelectContext(ctx, &movies, qry)

	if err != nil {
		return nil, fmt.Errorf("failed querying DB: %w", err)
	}

	return movies, nil
}

func (mdb *MovieDB) Get(ctx context.Context, id int) (model.Movie, error) {
	var m model.Movie

	qry := "select * from movies where id=?"

	err := mdb.DB.GetContext(ctx, &m, qry, id)

	if err != nil {
		return model.Movie{}, fmt.Errorf("unable to get movie with id %v: %w", id, err)
	}

	return m, nil
}

func (mdb *MovieDB) Create(ctx context.Context, movie model.Movie) (model.Movie, error) {

	return model.Movie{}, nil
}

func (mdb *MovieDB) Save(ctx context.Context, movie model.Movie) error {

	return nil
}
