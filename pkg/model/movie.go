package model

type Movie struct {
	ID    int    `json:"-" db:"id"`
	Title string `json:"title" db:"title"`
	Year  int    `json:"year" db:"year"`
}
