package route

import (
	"errors"
	"fmt"
	"movieapi/pkg/service"
	"net/http"
	"strconv"
)

var (
	ErrNotFound = errors.New("not found")
)

func CreateMovieMux(svc service.MovieService) *http.ServeMux {
	router := http.NewServeMux()

	router.HandleFunc("GET /movies", func(w http.ResponseWriter, r *http.Request) {

		ctx := r.Context()
		resp, err := svc.ListAll(r.Context(), service.ListAllMoviesRequest{
			Limit: 0,
			Page:  0,
		})

		if err == nil {
			if len(resp.Movies) == 0 {
				err = ErrNotFound
			}
		}

		writeJSON(ctx, resp, err, w)
	})

	router.HandleFunc("GET /movies/{id}", func(w http.ResponseWriter, request *http.Request) {
		id := request.PathValue("id")
		ctx := request.Context()

		i, err := strconv.Atoi(id)
		if err != nil {
			http.Error(w,
				fmt.Sprintf("invalid value for movie id: %s", id),
				http.StatusBadRequest)
			return
		}

		m, err := svc.Get(request.Context(), service.GetMovieRequest{
			ID: i,
		})

		writeJSON(ctx, m, err, w)

	})

	return router
}
