package route

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"io"
	"movieapi/pkg/model"
	"movieapi/pkg/service"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func createTestMoviesServer(mockSvc *service.MockMovieService) (*httptest.Server, string) {

	svr := httptest.NewServer(CreateMovieMux(mockSvc))
	addr := svr.Listener.Addr()

	return svr, fmt.Sprintf("http://%s", addr)
}

func Test_GetMovies(t *testing.T) {

	mockSvc := &service.MockMovieService{}

	mockSvc.On("ListAll", mock.Anything, mock.Anything).Return(&service.ListAllMoviesResponse{
		Movies: []model.Movie{
			{Title: "The Matrix", Year: 1999},
		},
	}, nil)

	svr, baseURL := createTestMoviesServer(mockSvc)
	defer svr.Close()

	u, _ := url.JoinPath(baseURL, "/movies")

	resp, err := http.Get(u)

	require.NoError(t, err)

	require.Equal(t, 200, resp.StatusCode)

	b, err := io.ReadAll(resp.Body)
	require.NoError(t, err)

	t.Log(string(b))
}

func Test_GetMoviesFails(t *testing.T) {

	mockSvc := &service.MockMovieService{}

	mockSvc.On("ListAll", mock.Anything, mock.Anything).Return(&service.ListAllMoviesResponse{},
		errors.New("ListAll failed"))

	svr, baseURL := createTestMoviesServer(mockSvc)
	defer svr.Close()

	u, _ := url.JoinPath(baseURL, "/movies")

	resp, err := http.Get(u)

	require.NoError(t, err)

	require.Equal(t, http.StatusInternalServerError, resp.StatusCode)

	b, err := io.ReadAll(resp.Body)
	require.NoError(t, err)

	t.Log(string(b))
}

func Test_GetMovie(t *testing.T) {

	mockSvc := &service.MockMovieService{}

	mockSvc.On("Get", mock.Anything, service.GetMovieRequest{
		ID: 1,
	}).Return(&service.GetMovieResponse{
		Movie: model.Movie{
			Title: "The Matrix", Year: 1999,
		},
	}, nil)

	svr, baseURL := createTestMoviesServer(mockSvc)
	defer svr.Close()

	t.Run("valid ID", func(t *testing.T) {
		u, _ := url.JoinPath(baseURL, "/movies/1")

		resp, err := http.Get(u)

		require.NoError(t, err)

		require.Equal(t, 200, resp.StatusCode)

		b, err := io.ReadAll(resp.Body)
		require.NoError(t, err)

		var gmr service.GetMovieRequest

		err = json.Unmarshal(b, &gmr)
		require.NoError(t, err)

		t.Log(string(b), gmr)
	})
	t.Run("invalid ID", func(t *testing.T) {
		u, _ := url.JoinPath(baseURL, "/movies/foo")

		resp, err := http.Get(u)

		require.NoError(t, err)

		require.Equal(t, http.StatusBadRequest, resp.StatusCode)

	})
}
