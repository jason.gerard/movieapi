package route

import (
	"context"
	"encoding/json"
	"log/slog"
	"net/http"
)

func writeJSON(ctx context.Context, o any, err error, w http.ResponseWriter) {
	if err == ErrNotFound {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if err != nil {
		slog.Error("error retrieving ", "error", err)
		http.Error(w,
			"unable to retrieve entity",
			http.StatusInternalServerError)
		return
	}

	b, err := json.Marshal(o)
	if err != nil {
		slog.Error("error in json.Marshal", "error", err)
		http.Error(w, "unable to retrieve entity", http.StatusInternalServerError)
		return
	}

	w.Header().Add("content-type", "application/json")

	w.Write(b)
}
