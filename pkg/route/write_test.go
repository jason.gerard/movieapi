package route

import (
	"context"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestWriteJSON(t *testing.T) {

	ctx := context.Background()
	var o any

	w := &httptest.ResponseRecorder{}

	t.Run("not found", func(t *testing.T) {
		writeJSON(ctx, o, ErrNotFound, w)

		require.Equal(t, http.StatusNotFound, w.Code)

	})
}
