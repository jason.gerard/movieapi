package service

import (
	"context"
	"github.com/stretchr/testify/mock"
)

type MockMovieService struct {
	mock.Mock
}

func (m *MockMovieService) ListAll(ctx context.Context, req ListAllMoviesRequest) (*ListAllMoviesResponse, error) {
	args := m.Called(ctx, req)
	return args.Get(0).(*ListAllMoviesResponse), args.Error(1)
}
func (m *MockMovieService) Get(ctx context.Context, req GetMovieRequest) (*GetMovieResponse, error) {
	args := m.Called(ctx, req)
	return args.Get(0).(*GetMovieResponse), args.Error(1)
}
