package service

import "movieapi/pkg/model"

type ListAllMoviesRequest struct {
	Limit int `json:"limit,omitempty"`
	Page  int `json:"page,omitempty"`
}

type ListAllMoviesResponse struct {
	Movies []model.Movie `json:"movies,omitempty"`
	Limit  int           `json:"limit,omitempty"`
	Page   int           `json:"page,omitempty"`
}

type GetMovieRequest struct {
	ID int `json:"id"`
}

type GetMovieResponse struct {
	Movie model.Movie
}
