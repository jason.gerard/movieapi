package service

import (
	"context"
	"fmt"
	"movieapi/pkg/db"
)

type MovieService interface {
	ListAll(ctx context.Context, req ListAllMoviesRequest) (*ListAllMoviesResponse, error)
	Get(ctx context.Context, req GetMovieRequest) (*GetMovieResponse, error)
}

type svc struct {
	db *db.MovieDB
}

func NewMovieService(db *db.MovieDB) MovieService {
	return &svc{
		db: db,
	}
}

func (s *svc) ListAll(ctx context.Context, req ListAllMoviesRequest) (*ListAllMoviesResponse, error) {

	var offset int
	if req.Page > 0 {
		offset = req.Limit * req.Page
	}

	movies, err := s.db.ListAll(ctx, req.Limit, offset)

	if err != nil {
		return nil, fmt.Errorf("error when trying to retrieve all movies form db: %w", err)
	}

	return &ListAllMoviesResponse{
		Movies: movies,
		Limit:  req.Limit,
		Page:   req.Page,
	}, nil

}

func (s *svc) Get(ctx context.Context, req GetMovieRequest) (*GetMovieResponse, error) {
	if req.ID < 1 {
		return nil, fmt.Errorf("invalid movie ID: %v", req.ID)
	}

	m, err := s.db.Get(ctx, req.ID)

	if err != nil {
		return nil, fmt.Errorf("unable to get movie: %w", err)
	}

	return &GetMovieResponse{
		Movie: m,
	}, nil
}

//func (s *svc) Create(ctx context.Context, req CreateMovieRequest) (*CreateMovieResponse, error) {
//	return
//}
//
//func (s *svc) Save(ctx context.Context, req SaveMovieRequest) (*SaveMovieResponse), error) {
//	return
//}
