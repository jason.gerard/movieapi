package service

import (
	"context"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
	"movieapi/pkg/db"
	"path"
	"testing"
)

func setupTestData(t *testing.T, db *db.MovieDB) {
	q := `insert into movies (title, year) values ('The Matrix', 1999),
		  ('Star Wars', 1977),
		  ('The Empire Strikes Back', 1981),
          ('Back to the Future', 1985);`

	_, err := db.DB.Exec(q)
	if err != nil {
		panic(err)
	}
}

func TestMovieService_ListAll(t *testing.T) {
	ctx := context.Background()

	config := viper.New()

	config.Set("SQLITE_PATH", path.Join(t.TempDir(), "test.sqlite"))

	db, err := db.GetMovieDB(ctx, config)
	require.NoError(t, err)

	setupTestData(t, db)

	svc := NewMovieService(db)

	resp, err := svc.ListAll(ctx, ListAllMoviesRequest{
		Limit: 1,
		Page:  2,
	})
	require.NoError(t, err)

	require.Len(t, resp.Movies, 1)
	require.Equal(t, resp.Movies[0].Title, "The Empire Strikes Back")
	require.Equal(t, resp.Movies[0].Year, 1981)

}

func TestGet(t *testing.T) {
	ctx := context.Background()

	config := viper.New()

	config.Set("SQLITE_PATH", path.Join(t.TempDir(), "test.sqlite"))

	db, err := db.GetMovieDB(ctx, config)
	require.NoError(t, err)

	setupTestData(t, db)

	svc := NewMovieService(db)

	t.Run("notfound", func(t *testing.T) {
		_, err = svc.Get(ctx, GetMovieRequest{ID: 0})
		require.Error(t, err)
	})

	t.Run("found", func(t *testing.T) {
		m, err := svc.Get(ctx, GetMovieRequest{ID: 1})
		require.NoError(t, err)
		require.Equal(t, "The Matrix", m.Movie.Title)
	})

}
